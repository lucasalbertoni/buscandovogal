package leituraarquivo;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe main principal.
 * Como o projeto � apenas para encontrar uma vogal foi feita dessa forma.
 * @author Lucas
 *
 */
public class Principal {
	
	/**
	 * Vogais poss�veis.
	 */
	private static String[] VOGAIS = new String[]{"a", "e", "i", "o", "u"};
	
	/**
	 * Vogais Utilizadas.
	 */
	private static String[] VOGAIS_USADAS = new String[10];
	
	/**
	 * Quando n�o for encontrado nenhuma vogal.
	 */
	private static String RETORNO_NAO_ENCONTRADO = "0";

	/**
	 * M�todo MAIN.
	 * @param args
	 */
	public static void main(String[] args) {
		//Resultado esperado no sysout
		//Vogal encontrada nas condi��es solicitadas: u
		StringBuilder sb = new StringBuilder("aAbBABacafeaAbUBABacafe");
		executar(sb);
		//Resultado esperado no sysout
		//Vogal encontrada nas condi��es solicitadas: e
		sb = new StringBuilder("aAbBABacafeaAbUBABacafe");
		executar(sb);
		//Resultado esperado no sysout
		//Vogal n�o encontrada nas condi��es solicitadas.
		sb = new StringBuilder("aAbBABacafeaAbUBABacafeu");
		executar(sb);
	}
	
	/**
	 * M�todo para criar a Stream e chamar o buscador de vogal.
	 * @param sb
	 */
	public static void executar(StringBuilder sb){
		Stream input = new StreamImpl();
		((StreamImpl)input).setStream(sb);
		char retorno = firstChar(input);
		if(RETORNO_NAO_ENCONTRADO.charAt(0) != retorno){
			System.out.println("Vogal encontrada nas condi��es solicitadas: "+retorno);
		}else{
			System.out.println("Vogal n�o encontrada nas condi��es solicitadas.");
		}
	}
	
	/**
	 * M�todo que busca a vogal utilizando o stream.
	 * @param input
	 * @return
	 */
	public static char firstChar(Stream input) {
		String primeiraPosicao = RETORNO_NAO_ENCONTRADO;
		String segundaPosicao = RETORNO_NAO_ENCONTRADO;
		List<String> possiveisRetornos = new ArrayList<String>();
		int vogaisInutilizadas = 0;
		while(input.hasNext()){
			String terceiraPosicao = String.valueOf(input.getNext());
			//Caso a vogal tenha sido encontrada e seja repetida eu zero.
			if(possiveisRetornos.contains(terceiraPosicao.toUpperCase())
					|| possiveisRetornos.contains(terceiraPosicao.toLowerCase())){
				possiveisRetornos.remove(terceiraPosicao.toLowerCase());
				possiveisRetornos.remove(terceiraPosicao.toUpperCase());
				VOGAIS_USADAS[vogaisInutilizadas] = terceiraPosicao;
			}else{
				//Vogal
				//Ap�s uma consoante
				//Vogal antecessora a consoante
				//Ultima vogal n�o se repete
				if(isVogal(terceiraPosicao) 
						&& !isVogal(segundaPosicao) 
						&& isVogal(primeiraPosicao) 
						&& !isVogalUsada(terceiraPosicao)){
					possiveisRetornos.add(terceiraPosicao);
				}else if(isVogal(terceiraPosicao) 
						&& !isVogalUsada(terceiraPosicao)){
					VOGAIS_USADAS[vogaisInutilizadas] = terceiraPosicao;
					vogaisInutilizadas++;
				}
			}
			primeiraPosicao = segundaPosicao;
			segundaPosicao = terceiraPosicao;
		}
		return (possiveisRetornos != null && possiveisRetornos.size()>0) ? possiveisRetornos.get(0).charAt(0) : '0';
	}
	
	/**
	 * M�todo que valida se a vogal esta usada.
	 * @param teste
	 * @return
	 */
	public static boolean isVogalUsada(String teste){
		for(String vogal : VOGAIS_USADAS){
			if(vogal != null 
					&& vogal.equalsIgnoreCase(teste)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * M�todo que valida se � vogal.
	 * @param teste
	 * @return
	 */
	public static boolean isVogal(String teste){
		for(String vogal : VOGAIS){
			if(vogal.equalsIgnoreCase(teste)){
				return true;
			}
		}
		return false;
	}

}
