package leituraarquivo;

/**
 * Interface para busca da vogal.
 * @author Lucas
 *
 */
public interface Stream {

	/**
	 * M�todo que retorna a pr�xima posi��o da stream
	 * @return
	 */
	public char getNext();
	
	/**
	 * M�todo que define se a stream tem uma pr�xima posi��o.
	 * @return
	 */
	public boolean hasNext();
	
}
