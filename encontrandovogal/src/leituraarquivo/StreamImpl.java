package leituraarquivo;

/**
 * Classe de implementa��o da classe stream.
 * @author Lucas
 *
 */
public class StreamImpl implements Stream{
	
	/**
	 * Atributo que n�o poder� ser recuperado, 
	 * <br />apenas definido uma vez e acessado pelo m�todo getNext()
	 */
	private StringBuilder stream; 

	@Override
	public char getNext() {
		char retorno = '0';
		if(hasNext()){
			retorno = stream.charAt(0);
			stream.deleteCharAt(0);
		}
		return retorno;
	}

	@Override
	public boolean hasNext() {
		return stream.length()>0;
	}

	public void setStream(StringBuilder stream) {
		this.stream = stream;
	}

}
